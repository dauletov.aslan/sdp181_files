package kz.astana.filesexample;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CODE = 100;
    private TextView textView;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },
                REQUEST_CODE
        );

        textView = findViewById(R.id.textView);

        SharedPreferences sp = getSharedPreferences("text", MODE_PRIVATE);
        String s = sp.getString("KEY_TEXT", "Vasya");
        textView.setText(s);

        Button writeToFile = findViewById(R.id.writeToFile);
        Button readFromFile = findViewById(R.id.readFromFile);
        Button writeToSdFile = findViewById(R.id.writeToSdFile);
        Button readFromSdFile = findViewById(R.id.readFromSdFile);
        Button createDirPublic = findViewById(R.id.createDirPublic);
        Button createDirStorage = findViewById(R.id.createDirStorage);

        writeToFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeToFile();
            }
        });
        readFromFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readFromFile();
            }
        });
        createDirPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file = createDirSd();
                saveToSd(file);
                readFromSd(file);
            }
        });
        createDirStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fileStorage = createDirStorage();
            }
        });

        Button open = findViewById(R.id.open);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SharedActivity.class));
            }
        });

        Button settings = findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            boolean isGranted = true;
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                    isGranted = true;
                } else {
                    isGranted = false;
                }
            }
            if (isGranted) {
                Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void writeToFile() {
        try {
            FileOutputStream fileOutputStream = openFileOutput("my.txt", MODE_PRIVATE);
            fileOutputStream.write("Hello Vasya Pupkin!".getBytes());
            fileOutputStream.close();
            textView.setText("File saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readFromFile() {
        try {
            FileInputStream inputStream = openFileInput("my.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String read;
            StringBuffer total = new StringBuffer();
            if ((read = bufferedReader.readLine()) != null) {
                total.append(read).append("\n");
            }
            textView.setText(total.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File createDirSd() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "secret");
        if (!file.mkdirs()) {
            Log.d("Hello", "File not created in sd");
        }
        return file;
    }

    private File createDirStorage() {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "file");
        if (!file.mkdirs()) {
            Log.d("Hello", "File not created in storage");
        }
        return file;
    }

    private void saveToSd(File file) {
        try {
            Log.d("Hello", "Path: " + file.getAbsolutePath());
            File outputFile = new File(file.getAbsolutePath(), "my.txt");
            Log.d("Hello", "Path: " + outputFile.getAbsolutePath());
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            fileOutputStream.write("Aslan".getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readFromSd(File file) {
        try {
            File inputFile = new File(file.getAbsolutePath(), "my.txt");
            FileInputStream fileInputStream = new FileInputStream(inputFile);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
            textView.setText(stringBuffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}