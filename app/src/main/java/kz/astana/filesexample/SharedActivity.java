package kz.astana.filesexample;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class SharedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        EditText editText = findViewById(R.id.editText);
        Button save = findViewById(R.id.save);
        Button load = findViewById(R.id.load);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("text", MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("KEY_TEXT", editText.getText().toString());
//                editor.remove("KEY_TEXT");
//                editor.clear();
                editor.commit();
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("text", MODE_PRIVATE);
                String s = sp.getString("KEY_TEXT", "");
                editText.setText(s);
            }
        });
    }
}